package com.softpath.beans;

public class Motos {

	private String marca;
	private String nombre;
	
	
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public void print(){
		System.out.println(marca+"  "+nombre);
	}
	
}
