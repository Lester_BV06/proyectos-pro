package com.softpath.beans;

public class Calculadora {

	private int num;
	
	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}
	
	public int sumatoria(int num){
		
		int res=0;
		for(int i = num;i>0;i--){
			res=res+i;
		}
		return res;
	}
	
	public int factorial(int num){
		
		int res=1;
		for(int i = num;i>0;i--){
			res=res*i;
		}
		return res;
	}
}
