package com.softpath.aspectos;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.weaver.JoinPointSignature;

@Aspect  //Configuramos todo lo que queremos hacer con aspectos
public class AutosAspects {

/*
 * Aqui se ejecutan todos los metodos que coincidan con print()
	@Before("execution(public void print())")
	public void beforeMethod(){
		
		System.out.println("Ejecutandose antes del metodo");
	}
*/
/*	
 * Aqui se ejecuta el metodo que esta en el paquete especificado
	@Before("execution(public void com.softpath.beans.Autos.print())")
	public void beforeMethod(){
		
		System.out.println("Ejecutandose antes del metodo");
	}
*/
/*	
 * Se ejecuta cualquier metodo del paquete que se llame print...
	@Before("execution(public void com.softpath.beans.Autos.print*())")
	public void beforeMethod(){
		
		System.out.println("Ejecutandose antes del metodo");
	}
*/
/* Ejecutamos cualquier metodo de la clase Autos	
	@Before("execution(public void com.softpath.beans.Autos.*())")
	public void beforeMethod(){
		
		System.out.println("Ejecutandose antes del metodo");
	}
*/	
/* Ejecutamos cualquier metodo de la clase Autos que regresa cualquier valor	
	@Before("allStringPrints() || allIntPrints()")
	public void beforeMethod(JoinPoint jointPoint){
		
		System.out.println("Ejecutandose antes del metodo"+jointPoint.getKind());
		System.out.println("Ejecutandose antes del metodo"+jointPoint.getArgs());
		System.out.println("Ejecutandose antes del metodo"+jointPoint.getSignature());
		System.out.println("Ejecutandose antes del metodo"+jointPoint.getSourceLocation());
		System.out.println("Ejecutandose antes del metodo"+jointPoint.getStaticPart());
		System.out.println("Ejecutandose antes del metodo"+jointPoint.getTarget());
	}
	
	@Pointcut("execution(public int com.softpath.beans.*.print*())")
	public void allIntPrints(){
		
	}
	
	@Pointcut("execution(public String com.softpath.beans.*.print*())")
	public void allStringPrints(){
		
	}
*/
/*	
	@AfterThrowing("workingWithArgs()")
	public void beforeMethod(JoinPoint jointPoint){
		System.out.println("Ejecutandose despues del metodo "+jointPoint.getSignature());
	}
	
	@Pointcut("args(String)")
	public void workingWithArgs(){
		
	}
*/	
	/*
	@AfterThrowing(pointcut="args(myvariable)", throwing = "lester")
	public void beforeMethod(String myvariable, RuntimeException lester){
		System.out.println("My argument is "+myvariable);
		System.out.println("The exception is "+lester);
	}
	
	@Pointcut("args(String)")
	public void workingWithArgs(){
		
	}
*/	
/*
	@AfterReturning(pointcut="args(myvariable)", returning = "lester")
	public void beforeMethod(String myvariable, double lester){
		System.out.println("My argument is "+myvariable);
		System.out.println("El valor de retorno es "+lester);
	}
	
	@Pointcut("args(String)")
	public void workingWithArgs(){
		
	}
*/
	
	
	@Pointcut("execution(public int com.softpath.beans.*.print*())")
	public void allIntPrints(){
		
	}
	
	@Pointcut("execution(public String com.softpath.beans.*.print*())")
	public void allStringPrints(){
		
	}
	
	@Pointcut("args(String)")
	public void workingWithArgs(){
		
	}
	/*Engloba tanto before como after */
	@Around("workingWithArgs()")
	public Object myRoundMethod(ProceedingJoinPoint jointPoint){
		
		Object metodo = null;
		try{
		System.out.println("Antes del metodo");
		metodo = jointPoint.proceed();
		System.out.println("Despues del metodo");
		}catch(Throwable e){
			System.out.println(e.getMessage());
		}
		System.out.println((Double)metodo);
		return metodo;
	}
}
