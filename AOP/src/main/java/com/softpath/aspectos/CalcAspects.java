package com.softpath.aspectos;

import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

@Aspect
public class CalcAspects {

	@AfterReturning(pointcut="execution(public int com.softpath.beans.Calculadora.*(int))",
			returning = "lester")
	public void beforeMethod(int lester){
		System.out.println("El resultado es: "+lester);
	}
	
//	@Before("execution(public void com.softpath.beans.Calculadora.*(int))")
//	public void beforeMethod(){
//		
//		System.out.println("Ejecutandose antes del metodo");
//	}
}
