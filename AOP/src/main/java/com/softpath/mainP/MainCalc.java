package com.softpath.mainP;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.softpath.beans.Calculadora;
import com.softpath.configurationd.CalcConfig;

public class MainCalc {

	public static void main(String[] args) {
		
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
		context.register(CalcConfig.class);
		context.refresh();
		Calculadora calcular = (Calculadora) context.getBean(Calculadora.class);
		calcular.sumatoria(5);
		calcular.factorial(5);
	}
}
