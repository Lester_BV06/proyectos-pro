package com.softpath.mainP;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.softpath.beans.Autos;
import com.softpath.beans.Motos;
import com.softpath.configurationd.AOPConfiguration;

public class MainAspect {

	public static void main(String[] args) {
		
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
		context.register(AOPConfiguration.class);
		context.refresh();
		Motos moto = (Motos) context.getBean(Motos.class);
		Autos auto = (Autos) context.getBean(Autos.class);
//		moto.print();
		auto.print("lester");
//		auto.print1();
//		auto.other();
		
	}
}
