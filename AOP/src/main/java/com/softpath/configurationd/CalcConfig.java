package com.softpath.configurationd;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

import com.softpath.aspectos.AutosAspects;
import com.softpath.aspectos.CalcAspects;
import com.softpath.beans.Calculadora;

@Configuration
@EnableAspectJAutoProxy
public class CalcConfig {

	@Bean
	public Calculadora createCalc(){
		Calculadora calcular = new Calculadora();
		return calcular;
	}
	
	@Bean
	public CalcAspects myAspect(){
		
		return new CalcAspects();
	}
}
