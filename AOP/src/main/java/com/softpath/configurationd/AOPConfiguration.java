package com.softpath.configurationd;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

import com.softpath.aspectos.AutosAspects;
import com.softpath.beans.Autos;
import com.softpath.beans.Motos;

@Configuration
@EnableAspectJAutoProxy
public class AOPConfiguration {

	@Bean
	public Autos createAuto(){
		
		Autos auto = new Autos();
		auto.setMarca("Jeep");
		auto.setNombre("Jeepster");
		return auto;
	}
	
	@Bean
	public AutosAspects myAspect(){
		
		return new AutosAspects();
	}
	
	@Bean
	public Motos createMoto(){
		
		Motos moto = new Motos();
		moto.setMarca("Yamaha");
		moto.setNombre("Ninja");
		return moto;
	}
	
}
