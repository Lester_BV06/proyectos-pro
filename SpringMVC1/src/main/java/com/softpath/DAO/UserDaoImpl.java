package com.softpath.DAO;

import javax.transaction.Transactional;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.softpath.entity.User;

@Repository
@Transactional
public class UserDaoImpl implements UserDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	
	@Override
	public User getUser(String username) {
		return (User)sessionFactory.getCurrentSession().
		createQuery("from User where username =:myusername")
		.setParameter("myusername", username).uniqueResult();	
	}

	
}
