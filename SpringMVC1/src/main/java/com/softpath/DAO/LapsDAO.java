package com.softpath.DAO;

import java.util.List;

import com.softpath.entity.Laptops;

public interface LapsDAO {

	public List<Laptops> getAllLaps();
	public Laptops getLap(long id);
	public void updateLap(Laptops lap);
	public void deleteLap(long id);
}
