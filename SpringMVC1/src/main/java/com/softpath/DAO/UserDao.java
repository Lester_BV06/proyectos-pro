package com.softpath.DAO;

import com.softpath.entity.User;

public interface UserDao {

	public User getUser(String username);
	
}
