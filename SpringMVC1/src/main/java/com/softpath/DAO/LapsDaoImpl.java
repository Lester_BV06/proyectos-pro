package com.softpath.DAO;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.softpath.entity.Laptops;

@Repository
@Transactional
public class LapsDaoImpl implements LapsDAO {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<Laptops> getAllLaps() {
		Criteria crit = sessionFactory.getCurrentSession().createCriteria(Laptops.class);
		List<Laptops> list = crit.list();
		return list;
	}

	@Override
	public Laptops getLap(long id) {
		Laptops lap = (Laptops) sessionFactory.getCurrentSession().get(Laptops.class, id);
		return lap;
	}

	@Override
	public void updateLap(Laptops lap) {
		sessionFactory.getCurrentSession().saveOrUpdate(lap);
	}

	@Override
	public void deleteLap(long id) {
		sessionFactory.getCurrentSession().createQuery
		("delete from Laptops where id =:myid").setParameter("myid", id).executeUpdate();
	}

	
}
