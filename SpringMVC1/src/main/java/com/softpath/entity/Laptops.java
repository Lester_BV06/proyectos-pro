package com.softpath.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name="Laptop")
public class Laptops {

	@Id@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	@NotEmpty(message="El campo marca no puede estar vacio")
	@Size(min=2, max=20,message="El nombre de la marca no es valido")
	@Column(nullable=false)
	private String marca;
	@Digits(integer=5, fraction=2, message="El lado entero solo puede tener 5 digitos y el decimal 2")
	private double precio;
	private int hdd;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}

	public double getPrecio() {
		return precio;
	}
	public void setPrecio(double precio) {
		this.precio = precio;
	}
	public int getHdd() {
		return hdd;
	}
	public void setHdd(int hdd) {
		this.hdd = hdd;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	
	
	
}
