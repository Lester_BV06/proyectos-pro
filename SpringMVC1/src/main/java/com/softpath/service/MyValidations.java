package com.softpath.service;

import java.util.Set;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.softpath.entity.Laptops;

public class MyValidations implements Validator {

	@Override
	public boolean supports(Class<?> class1) {
		return Laptops.class.isAssignableFrom(class1);
	}

	@Override
	public void validate(Object targetClass, Errors error) {
	
		Laptops laps = (Laptops)targetClass;
		
		if(laps.getHdd() < 60)
			error.rejectValue("hdd", "hdd.error");
	}

	
}
