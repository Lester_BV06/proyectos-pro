package com.softpath.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import com.softpath.DAO.UserDao;
import com.softpath.entity.User;

@Service("customUserDetailsService")
public class LoginServiceUser implements UserDetailsService{

	@Autowired
	private UserDao userdao;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException 
	{
	User useru = userdao.getUser(username);
	System.out.println(useru.getUsername() + " " + useru.getRole());
	
	if(useru == null)
		throw new UsernameNotFoundException("Usuario no encontrado");
	else
		return new org.springframework.security.
				core.userdetails.User(useru.getUsername(), useru.getPassword(), true,true,true
						,true,getAuto(useru));
			
	}

	private List<GrantedAuthority> getAuto(User user)
	{
		List<GrantedAuthority> auto = new ArrayList<>();
		auto.add(new SimpleGrantedAuthority(user.getRole()));
		return auto;
	}
	
	
}
