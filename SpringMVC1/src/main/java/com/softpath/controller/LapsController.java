package com.softpath.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.softpath.DAO.LapsDAO;
import com.softpath.entity.Laptops;
import com.softpath.service.MyValidations;

@Controller
public class LapsController {

	@ModelAttribute("lapModel")
	public Laptops modelLap() {
		return new Laptops();
	}

	@Autowired
	private LapsDAO dao;

	@RequestMapping(value ="/showAll", method = RequestMethod.GET)
	public String listaLaps(ModelMap model) {
		model.addAttribute("lapsmodel", dao.getAllLaps());
		return "laps";
	}
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String welcome(ModelMap model) {
		model.addAttribute("lapsmodel", dao.getAllLaps());
		return "welcome";
	}

	@RequestMapping(value = "/update-{myid}", method = RequestMethod.GET)
	public String actualizarLap(@PathVariable long myid, ModelMap model) {
		model.addAttribute("laptop", dao.getLap(myid));
		return "update";
	}

	@RequestMapping(value = "/delete-{myid}", method = RequestMethod.GET)
	public String doDelete(@PathVariable long myid, ModelMap model) {
		dao.deleteLap(myid);
		return "success";
	}

	@RequestMapping(value = "/updateSingleLap", method = RequestMethod.POST)
	public String doActualizar(@Valid @ModelAttribute("lapModel") Laptops laps, BindingResult result) {
		
		MyValidations myValidations = new MyValidations();
		myValidations.validate(laps,result);
		
		if (result.hasErrors()) {
			return "update";
		} else {
			dao.updateLap(laps);
			return "success";
		}
	}

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public String createLap() {
		return "update";
	}
	
	@RequestMapping(value = "/showLaps", method = RequestMethod.GET)
	public String useraccessLap(ModelMap model) {
		model.addAttribute("lapsmodel", dao.getAllLaps());
		return "lapsuser";
	}

	@RequestMapping(value = "/login", method = {RequestMethod.POST, RequestMethod.GET})
	public String loginMethod() {
		return "login";
	}
	@RequestMapping(value = "/refusedpage", method = {RequestMethod.POST, RequestMethod.GET})
	public String errorPage() {
		return "errorpage";
	}
	
}
