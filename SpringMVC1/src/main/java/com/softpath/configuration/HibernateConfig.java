package com.softpath.configuration;


import java.util.Properties;

import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@ComponentScan({"com.softpath"})
public class HibernateConfig {

	@Bean
	public DataSource dataSource()
	{
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		dataSource.setUrl("jdbc:mysql://localhost:3306/hibernateSpring");
		dataSource.setUsername("root");
		dataSource.setPassword("K0m0d006");
		return dataSource;
	}
	
	@Bean
	public LocalSessionFactoryBean sessionFactory()
	{
		LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
		sessionFactory.setDataSource(dataSource());
		Properties pro = new Properties();
		pro.put("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
		pro.put("hibernate.hbm2ddl.auto","update");
		pro.put("hibernate.show_sql", "true");
		sessionFactory.setHibernateProperties(pro);
		sessionFactory.setPackagesToScan("com.softpath.entity");
		return sessionFactory;
	}
	
	@Bean
	@Autowired
	public HibernateTransactionManager transactionManager(SessionFactory sf)
	{
		HibernateTransactionManager manager = new HibernateTransactionManager();
		manager.setSessionFactory(sf);
		return manager;
	}
	
}
