<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<c:url var="logina" value="login"></c:url>
	<form action="${logina}" method="post">
		<table>
			<thead>
				<tr>
					<td>Nombre</td>
					<td><input type="text" name="username" id="username"></td>
				</tr>
				<tr>
					<td>Password</td>
					<td><input type="password" name="password" id="password"></td>
				</tr>
				<tr>
					<td><input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"></td>
					<td><input type="submit" value="loguear"></td>
				</tr>
			</thead>
		</table>
	</form>
</body>
</html>