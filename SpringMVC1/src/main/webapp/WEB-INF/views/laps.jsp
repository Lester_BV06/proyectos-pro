<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<title>Insert title here</title>
</head>
<body>
	<table class="table table-hover">
		<thead>
			<tr class="success">
				<td>Marca</td>
				<td>Precio</td>
				<td>Disco Duro</td>
				<td>Actualizar</td>
				<td>Borrar</td>
			</tr>
		</thead>
		<c:forEach items="${lapsmodel}" var="laps">
			<tr class="warning">
				<td>${laps.marca}</td>
				<td>${laps.precio}</td>
				<td>${laps.hdd}</td>			
				<td><a href="<c:url value='/update-${laps.id}' />">update</a></td>
				<td><a href="<c:url value='/delete-${laps.id}' />">delete</a></td>
			</tr>
		</c:forEach>
	</table>
	<h3><a href="<c:url value='/create' />">Add new Lap</a></h3>
</body>
</html>