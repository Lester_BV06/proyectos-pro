<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<title>Insert title here</title>
</head>
<body>
	<form:form action="updateSingleLap" method="POST"
		modelAttribute="lapModel">
		<form:input type="hidden" path="id" value="${laptop.id}" />
		<table class="table table-hover">
			<tr>
				<td>Marca</td>
				<td><form:input path="marca" value="${laptop.marca}" /></td>
				<td><form:errors path="marca" /></td>
			</tr>
			<tr>
				<td>Precio</td>
				<td><form:input path="precio" value="${laptop.precio}" /></td>
				<td><form:errors path="precio" /></td>
			</tr>
			<tr>
				<td>Disco Duro</td>
				<td><form:input path="hdd" value="${laptop.hdd}" /></td>
				<td><form:errors path="hdd"/></td>
			</tr>
			<tr>
				<td><input type="submit" value="Actualizar"></td>
			</tr>
		</table>
	</form:form>
</body>
</html>